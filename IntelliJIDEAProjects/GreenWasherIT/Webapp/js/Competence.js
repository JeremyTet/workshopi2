function createTooltip(self, gameObject, text, position) {
    const textPadding = 20;

    const textObj = self.add.text(textPadding, textPadding, text, { color: '#000' });
    const background = self.add.rectangle(0, 0, textObj.displayWidth + (textPadding * 2), textObj.displayHeight + (textPadding * 2), 0xffffff).setOrigin(0, 0);

    let container;
    if (position === 'right') {
        const tooltipX = gameObject.getRightCenter().x + 20; // Afficher à droite
        const tooltipY = gameObject.y - 20;
        container = self.add.container(tooltipX, tooltipY);
    } else if (position === 'left') {
        const tooltipX = gameObject.getLeftCenter().x - textObj.displayWidth - 20; // Afficher à gauche
        const tooltipY = gameObject.y - 20;
        container = self.add.container(tooltipX, tooltipY);
    } else {
        // Position par défaut (à droite)
        const tooltipX = gameObject.getRightCenter().x + 20;
        const tooltipY = gameObject.y - 20;
        container = self.add.container(tooltipX, tooltipY);
    }

    container.add(background);
    container.add(textObj);

    // Définir la profondeur (z-index) pour afficher au premier plan
    container.setDepth(1);

    container.setVisible(false);

    return container;
}



function loadCompetences(self) {
    return new Promise((resolve, reject) => {
        // Obtenir les dimensions de la scène
        const sceneWidth = self.sys.game.config.width;
        const sceneHeight = self.sys.game.config.height;

        // Calculer les coordonnées du centre de la scène
        const centerX = sceneWidth / 2;
        const centerY = sceneHeight / 2;

        // Créer l'image centrée
        const fond = self.add.image(centerX, centerY, 'fondGris')

        // Créer la croix pour fermer l'image
        const closeButton = self.add.image(centerX + (fond.displayWidth / 2), centerY - (fond.displayHeight / 2), 'Croix');

        // Définir la largeur et la hauteur personnalisées pour la croix
        const largeurCroix = 20; // Remplacez par la largeur souhaitée en pixels
        const hauteurCroix = 20; // Remplacez par la hauteur souhaitée en pixels
        closeButton.setDisplaySize(largeurCroix, hauteurCroix);

        closeButton.setInteractive(); // Rendre le bouton interactif

        // Créer les bâtiments
        const buildings = [];

        // Ajouter le bâtiment 1
        const Building1 = self.add.image(300, 550, 'Building1').setInteractive();
        const tooltip1 = createTooltip(self, Building1, "Utilisation de construction écologique dans un contexte Green IT\nGains : 35% XP","right");
        buildings.push({ image: Building1, tooltip: tooltip1 });

        // Ajouter le bâtiment 2
        const Building2 = self.add.image(200, 450, 'Building2').setInteractive();
        const tooltip2 = createTooltip(self, Building2, "Énergie solaire\nLes panneaux solaires convertissent l'énergie solaire en électricité,\nréduisant ainsi la dépendance aux combustibles fossiles et les émissions de carbone.","right");
        buildings.push({ image: Building2, tooltip: tooltip2 });
        
        // Ajouter le bâtiment 3
        const Building3 = self.add.image(300, 450, 'Building3').setInteractive();
        const tooltip3 = createTooltip(self, Building3, "Éoliennes\nLes éoliennes produisent de l'électricité à partir de l'énergie éolienne,\nune source d'énergie renouvelable qui ne génère pas de pollution atmosphérique.","right");
        buildings.push({ image: Building3, tooltip: tooltip3 });
        
        // Ajouter le bâtiment 4
        const Building4 = self.add.image(400, 450, 'Building4').setInteractive();
        const tooltip4 = createTooltip(self, Building4, "Agriculture verticale\nLes fermes verticales utilisent des systèmes hydroponiques et d'éclairage LED pour cultiver des cultures en milieu urbain,\nréduisant ainsi la nécessité de transporter des produits sur de longues distances.","right");
        buildings.push({ image: Building4, tooltip: tooltip4 });

        // Ajouter le bâtiment 5
        const Building5 = self.add.image(200, 350, 'Building5').setInteractive();
        const tooltip5 = createTooltip(self, Building5, "Réfrigération à absorption solaire\nL'énergie solaire alimente des réfrigérateurs et des climatiseurs, \noffrant une alternative écologique aux systèmes de réfrigération traditionnels.","right");
        buildings.push({ image: Building5, tooltip: tooltip5 });
        
        // Ajouter le bâtiment 6
        const Building6 = self.add.image(300, 350, 'Building6').setInteractive();
        const tooltip6 = createTooltip(self, Building6, "Éclairage LED\nLes ampoules LED sont plus économes en énergie que les ampoules traditionnelles,\nréduisant ainsi la consommation d'électricité et les émissions de gaz à effet de serre.","right");
        buildings.push({ image: Building6, tooltip: tooltip6 });
        
        // Ajouter le bâtiment 7
        const Building7 = self.add.image(400, 350, 'Building7').setInteractive();
        const tooltip7 = createTooltip(self, Building7, "Toitures végétalisées\nRéduction de l'écoulement des eaux de pluie, favorisation de la biodiversité urbaine et amélioration de l'efficacité énergétique des bâtiments.","right");
        buildings.push({ image: Building7, tooltip: tooltip7 });
        
        // Ajouter le bâtiment 8
        const Building8 = self.add.image(300, 250, 'Building8').setInteractive();
        const tooltip8 = createTooltip(self, Building8, "Impression 3D écologique\nLes imprimantes 3D utilisant des matériaux recyclables contribuent \nà réduire les déchets plastiques et encouragent la réutilisation des matériaux.","right");
        buildings.push({ image: Building8, tooltip: tooltip8 });

        // Ajouter  1 IA
        const IA1 = self.add.image(700, 350, 'IA1').setInteractive();
        const IATooltip1 = createTooltip(self, IA1, "Utilisation de l'IA dans un contexte Green IT","right");
        buildings.push({ image: IA1, tooltip: IATooltip1 });
        
        // Ajouter  2 IA
        const IA2 = self.add.image(600, 250, 'IA2').setInteractive();
        const IATooltip2 = createTooltip(self, IA2, " Systèmes d'alerte précoce pour les catastrophes naturelles\nTechnologies de surveillance avec des capteurs sismiques et des systèmes de prévision météorologique avancés","right");
        buildings.push({ image: IA2, tooltip: IATooltip2 });
        
        // Ajouter  3 IA
        const IA3 = self.add.image(800, 250, 'IA3').setInteractive();
        const IATooltip3 = createTooltip(self, IA3, " Aide de l'intelligence artificielle pour la consommation d'énergie\nUtilisation de la puissance de l'intelligence artificielle pour superviser et réguler la consommation.","right");
        buildings.push({ image: IA3, tooltip: IATooltip3 });

        // Ajouter  1 Bioénergie
        const BioEnergie1 = self.add.image(1100, 550, 'Bioenergie1').setInteractive();
        const BioEnergieTooltip1 = createTooltip(self, BioEnergie1, "Utilisation des bioénergies dans le contexte de Green IT","left");
        buildings.push({ image: BioEnergie1, tooltip: BioEnergieTooltip1 });
        
        // Ajouter  2 Bioénergie
        const BioEnergie2 = self.add.image(1000, 450, 'Bioenergie2').setInteractive();
        const BioEnergieTooltip2 = createTooltip(self, BioEnergie2, "Impression 3D écologique\nLes imprimantes 3D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie2, tooltip: BioEnergieTooltip2 });
        
        // Ajouter  3 Bioénergie
        const BioEnergie3 = self.add.image(1100, 450, 'Bioenergie3').setInteractive();
        const BioEnergieTooltip3 = createTooltip(self, BioEnergie3, "Impression 3D écologique\nLes imprimantes 3D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie3, tooltip: BioEnergieTooltip3 });
        
        // Ajouter  4 Bioénergie
        const BioEnergie4 = self.add.image(1200, 450, 'Bioenergie4').setInteractive();
        const BioEnergieTooltip4 = createTooltip(self, BioEnergie4, "Impression 4D écologique\nLes imprimantes 4D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie4, tooltip: BioEnergieTooltip4 });
        
        // Ajouter  5 Bioénergie
        const BioEnergie5 = self.add.image(1000, 350, 'Bioenergie6').setInteractive();
        const BioEnergieTooltip5 = createTooltip(self, BioEnergie5, "Impression 5D écologique\nLes imprimantes 5D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie5, tooltip: BioEnergieTooltip5 });
        
        // Ajouter  6 Bioénergie
        const BioEnergie6 = self.add.image(1100, 350, 'Bioenergie7').setInteractive();
        const BioEnergieTooltip6 = createTooltip(self, BioEnergie6, "Impression 6D écologique\nLes imprimantes 6D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie6, tooltip: BioEnergieTooltip6 });
        
        // Ajouter  7 Bioénergie
        const BioEnergie7 = self.add.image(1200, 350, 'Bioenergie8').setInteractive();
        const BioEnergieTooltip7 = createTooltip(self, BioEnergie7, "Impression 7D écologique\nLes imprimantes 7D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie7, tooltip: BioEnergieTooltip7 });
        
        // Ajouter  8 IA
        const BioEnergie8 = self.add.image(1000, 250, 'Bioenergie5').setInteractive();
        const BioEnergieTooltip8 = createTooltip(self, BioEnergie8, "Impression 8D écologique\nLes imprimantes 8D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie8, tooltip: BioEnergieTooltip8 });
        
        // Ajouter  9 IA
        const BioEnergie9 = self.add.image(1100, 250, 'Bioenergie9').setInteractive();
        const BioEnergieTooltip9 = createTooltip(self, BioEnergie9, "Impression 9D écologique\nLes imprimantes 9D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie9, tooltip: BioEnergieTooltip9 });
        
        // Ajouter  10 IA
        const BioEnergie10 = self.add.image(1100, 150, 'Bioenergie10').setInteractive();
        const BioEnergieTooltip10 = createTooltip(self, BioEnergie10, "Impression 10D écologique\nLes imprimantes 10D utilisant des matériaux recyclables contribuent à réduire les déchets plastiques et encouragent la réutilisation des matériaux.","left");
        buildings.push({ image: BioEnergie10, tooltip: BioEnergieTooltip10 });

        buildings.forEach((building) => {
            building.image.on('pointerover', () => {
                building.tooltip.setVisible(true);
            });

            building.image.on('pointerout', () => {
                building.tooltip.setVisible(false);
            });
        });

        closeButton.on('pointerdown', () => {
            // Fermez l'image ou effectuez toute autre action de fermeture
            fond.destroy();
            buildings.forEach((building) => {
                building.image.destroy();
                building.tooltip.destroy();
            });
            closeButton.destroy();
        });

        resolve();
    });
}
